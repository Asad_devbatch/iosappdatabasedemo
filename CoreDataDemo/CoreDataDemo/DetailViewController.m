//
//  DetailViewController.m
//  CoredataDemo
//
//  Created by Asad Ali on 04/12/2014.
//  Copyright (c) 2014 DevBatch. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UISwitch *statusSwitch;



@end

@implementation DetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Buttons Action

- (IBAction)btnSaveClicked:(id)sender
{
    if (!self.event)
    {
        self.event = (Event *)[Event create];
    }
    
    self.event.details = self.txtDescription.text;
    self.event.title = self.txtTitle.text;
    self.event.timeStamp = self.datePicker.date;
    self.event.isActive = @(self.statusSwitch.isOn);
    
    [Event save];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Managing the detail item

- (void)configureView
{
    // Update the user interface for the detail item.
    if (self.event)
    {
        self.txtDescription.text = self.event.details;
        self.txtTitle.text = self.event.title;
        self.datePicker.date = self.event.timeStamp;
        [self.statusSwitch setOn:[self.event.isActive boolValue]];
    }
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtTitle)
    {
        [self.txtDescription becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}


@end
