//
//  AppDelegate.h
//  CoredataDemo
//
//  Created by Asad Ali on 04/12/2014.
//  Copyright (c) 2014 DevBatch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHStorageHelper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

