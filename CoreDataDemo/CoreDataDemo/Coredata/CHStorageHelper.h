//
//  CHStorageHelper.h
//  ContactHome
//
//  Created by Asad Ali on 04/10/2014.
//  Copyright (c) 2014 DevBatch pvt Ltd. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CHStorageHelper : NSManagedObject

#define DbName @"contacts_db.sqlite"

#pragma mark - Core Data Stack

+ (NSManagedObjectContext *) managedObjectContext;
+ (NSPersistentStoreCoordinator *) persistentStoreCoordinator;
+ (NSManagedObjectModel *) managedObjectModel;
+ (NSString *) applicationDocumentsDirectory;



#pragma mark - IO Operations

+ (NSManagedObject *)create;
+ (void) save;

+ (void) deleteObject:(NSManagedObject *)object;
+ (void) deleteAllObjects;

+ (NSArray *) fetchAll;
+ (NSArray *) fetchWithPredicate:(NSPredicate *)filter sortDescriptor:(NSArray *)sortDescriptors fetchLimit:(int)limit;
+ (NSArray *) fetchWithEntity:(NSString *)entity predicate:(NSPredicate *)filter sortDescriptor:(NSArray *)sortDescriptors fetchLimit:(int)limit;

+ (NSInteger) count;

+ (int) maxIdOfAttributs:(NSString *)attribute;

+ (id)isManagedObjectAlreadyExist:(NSString *)entityName withPredicate:(NSPredicate *)filter;
+ (id)isManagedObjectAlreadyExist:(NSString *)entityName withPredicate:(NSPredicate *)filter inContext:(NSManagedObjectContext *)objContext;

@end
