//
//  Event.h
//  CoredataDemo
//
//  Created by Asad Ali on 04/12/2014.
//  Copyright (c) 2014 DevBatch. All rights reserved.
//

#import "CHStorageHelper.h"


@interface Event : CHStorageHelper

@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSString * details;

@end
