//
//  Event.m
//  CoredataDemo
//
//  Created by Asad Ali on 04/12/2014.
//  Copyright (c) 2014 DevBatch. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic timeStamp;
@dynamic title;
@dynamic isActive;
@dynamic details;

@end
