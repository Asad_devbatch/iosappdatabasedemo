//
//  CHStorageHelper.m
//  ContactHome
//
//  Created by Asad Ali on 04/10/2014.
//  Copyright (c) 2014 DevBatch pvt Ltd. All rights reserved.
//

#import "CHStorageHelper.h"

@implementation CHStorageHelper

#pragma mark - Core Data Stack

+ (NSManagedObjectContext *) managedObjectContext
{
    static NSManagedObjectContext* managedObjectContext_ = nil;
    
    if (managedObjectContext_ != nil)
    {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [[self class] persistentStoreCoordinator];
    
    if (coordinator != nil)
    {
        managedObjectContext_ = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        managedObjectContext_.persistentStoreCoordinator = coordinator;
    }
    
    return managedObjectContext_;
}


+ (NSPersistentStoreCoordinator *) persistentStoreCoordinator
{
    static NSPersistentStoreCoordinator* persistentStoreCoordinator_ = nil;
    
    if (persistentStoreCoordinator_ != nil)
    {
        return persistentStoreCoordinator_;
    }
    
    NSURL *storeURL = [NSURL fileURLWithPath: [[[self class] applicationDocumentsDirectory] stringByAppendingPathComponent:DbName]];
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@(YES), NSInferMappingModelAutomaticallyOption:@(YES)};
    
    NSLog(@"Db Path URL: %@",storeURL);
    
    NSError *error = nil;
    
    persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[[self class] managedObjectModel]];
    
    if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();// Comment this line if not in development mode
    }
    
    return persistentStoreCoordinator_;
}


+ (NSManagedObjectModel *) managedObjectModel
{
    static NSManagedObjectModel* managedObjectModel_ = nil;
    
    if (managedObjectModel_ != nil)
    {
        return managedObjectModel_;
    }
    
    managedObjectModel_ = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return managedObjectModel_;
}

+ (NSString *) applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}



#pragma mark - IO Operations

+ (NSManagedObject *)create
{
    NSString *entityName = [NSString stringWithFormat:@"%@",[self class]];
    
    NSManagedObject *obj = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[[self class] managedObjectContext]];
    
    return obj;
}

+ (void) save
{
    [[[self class] managedObjectContext] performBlock:^{
       
        NSError *error;
        if (![[[self class] managedObjectContext] save:&error])
        {
            NSLog(@"Error in Saving: %@", error.localizedDescription);
        }
    }];
}

+ (void) deleteObject:(NSManagedObject *)object
{
    [[self managedObjectContext] deleteObject:object];
    
    [[self class] save];
}

+ (void) deleteAllObjects
{
    for (NSManagedObject *obj in [[self class] fetchAll])
    {
        [[[self class] managedObjectContext] deleteObject:obj];
    }
    
    [[self class] save];
}

+ (NSArray *) fetchAll
{
    return [[self class] fetchWithPredicate:nil sortDescriptor:nil fetchLimit:0];
}

+ (NSArray *) fetchWithPredicate:(NSPredicate *)filter sortDescriptor:(NSArray *)sortDescriptors fetchLimit:(int)limit
{
    NSString *entityName = [NSString stringWithFormat:@"%@",[self class]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:[[self class] managedObjectContext]];
    [fetchRequest setFetchBatchSize:30];
    [fetchRequest setEntity:entity];
    
    if (filter != nil)
    {
        [fetchRequest setPredicate:filter];
    }
    if (sortDescriptors != nil)
    {
        [fetchRequest setSortDescriptors:sortDescriptors];
    }
    if (limit > 0)
    {
        [fetchRequest setFetchLimit:limit];
    }
    
    NSError *error = nil;
    
    NSArray *result = [[[self class] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    if (!result)
    {
        NSLog(@"Error in Getting Core-Data Record: %@", error.localizedDescription);
        return nil;
    }
    
    return result;
}

+ (NSArray *) fetchWithEntity:(NSString *)entityName predicate:(NSPredicate *)filter sortDescriptor:(NSArray *)sortDescriptors fetchLimit:(int)limit
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:[[self class] managedObjectContext]];
    [fetchRequest setFetchBatchSize:30];
    [fetchRequest setEntity:entity];
    
    if (filter != nil)
    {
        [fetchRequest setPredicate:filter];
    }
    if (sortDescriptors != nil)
    {
        [fetchRequest setSortDescriptors:sortDescriptors];
    }
    if (limit > 0)
    {
        [fetchRequest setFetchLimit:limit];
    }
    
    NSError *error = nil;
    
    NSArray *result = [[[self class] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    if (!result)
    {
        NSLog(@"Error in Getting Core-Data Record: %@", error.localizedDescription);
        return nil;
    }
    
    return result;
}

+ (NSInteger) count
{
    NSFetchRequest *fr = [NSFetchRequest fetchRequestWithEntityName:[NSString stringWithFormat:@"%@",[self class]]]
    ;
    __block NSUInteger rCount = 0;
    
    [[[self class] managedObjectContext] performBlockAndWait:^()
    {
        NSError *error;
        rCount = [[[self class] managedObjectContext] countForFetchRequest:fr error:&error];
        
        if (rCount == NSNotFound) {
            NSLog(@"Error in Retrieving items: %@", error.localizedDescription);
        } }];

    NSLog(@"Retrieved %d items", (int)rCount);
    return rCount;
}

+ (int) maxIdOfAttributs:(NSString *)attribute
{
    NSString *entityName = [NSString stringWithFormat:@"%@",[self class]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
    
    fetchRequest.fetchLimit = 1;
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:attribute ascending:NO]];
    
    NSError *error = nil;
    
    NSManagedObject *objNSManagedObject = [[[self class] managedObjectContext] executeFetchRequest:fetchRequest error:&error].lastObject;
    
    if (objNSManagedObject)
    {
        return [[objNSManagedObject valueForKey:attribute] intValue];
    }
    
    return 0;
}

+ (id)isManagedObjectAlreadyExist:(NSString *)entityName withPredicate:(NSPredicate *)filter
{
    return [CHStorageHelper isManagedObjectAlreadyExist:entityName withPredicate:filter inContext:[CHStorageHelper managedObjectContext]];
}

+ (id)isManagedObjectAlreadyExist:(NSString *)entityName withPredicate:(NSPredicate *)filter inContext:(NSManagedObjectContext *)objContext
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:objContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchLimit:1];
    [fetchRequest setPredicate:filter];
    
    NSError *error = nil;
    NSArray *result = [objContext executeFetchRequest:fetchRequest error:&error];
    
    return [result lastObject];
}

@end
